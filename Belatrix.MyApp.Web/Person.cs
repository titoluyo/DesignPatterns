﻿namespace Belatrix.MyApp.Web
{
    // Simulate a Entity
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
