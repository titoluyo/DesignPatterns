﻿namespace Belatrix.MyApp.Web
{
    // Simulate a DTO
    public class PersonDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
