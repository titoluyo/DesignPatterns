﻿namespace Belatrix.Factory.Tests.Factory
{
    public class Profile : AutoMapper.Profile
    {
        public Profile()
        {
            CreateMap<Person, PersonDTO>();
        }
    }
}
